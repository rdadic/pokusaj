﻿namespace WindowsFormsApp9
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.polje = new System.Windows.Forms.TextBox();
            this.plus = new System.Windows.Forms.Button();
            this.minus = new System.Windows.Forms.Button();
            this.mnozenje = new System.Windows.Forms.Button();
            this.dijeljenje = new System.Windows.Forms.Button();
            this.korijen = new System.Windows.Forms.Button();
            this.sinus = new System.Windows.Forms.Button();
            this.kosinus = new System.Windows.Forms.Button();
            this.nat_log = new System.Windows.Forms.Button();
            this.logaritam = new System.Windows.Forms.Button();
            this.izracunaj = new System.Windows.Forms.Button();
            this.clear = new System.Windows.Forms.Button();
            this.exit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // polje
            // 
            this.polje.Location = new System.Drawing.Point(315, 108);
            this.polje.Name = "polje";
            this.polje.Size = new System.Drawing.Size(131, 20);
            this.polje.TabIndex = 0;
            this.polje.TextChanged += new System.EventHandler(this.polje_TextChanged);
            // 
            // plus
            // 
            this.plus.Location = new System.Drawing.Point(242, 230);
            this.plus.Name = "plus";
            this.plus.Size = new System.Drawing.Size(75, 23);
            this.plus.TabIndex = 16;
            this.plus.Text = "+";
            this.plus.UseVisualStyleBackColor = true;
            this.plus.Click += new System.EventHandler(this.plus_Click);
            // 
            // minus
            // 
            this.minus.Location = new System.Drawing.Point(242, 269);
            this.minus.Name = "minus";
            this.minus.Size = new System.Drawing.Size(75, 23);
            this.minus.TabIndex = 17;
            this.minus.Text = "-";
            this.minus.UseVisualStyleBackColor = true;
            this.minus.Click += new System.EventHandler(this.minus_Click);
            // 
            // mnozenje
            // 
            this.mnozenje.Location = new System.Drawing.Point(242, 316);
            this.mnozenje.Name = "mnozenje";
            this.mnozenje.Size = new System.Drawing.Size(75, 23);
            this.mnozenje.TabIndex = 18;
            this.mnozenje.Text = "*";
            this.mnozenje.UseVisualStyleBackColor = true;
            this.mnozenje.Click += new System.EventHandler(this.mnozenje_Click);
            // 
            // dijeljenje
            // 
            this.dijeljenje.Location = new System.Drawing.Point(242, 357);
            this.dijeljenje.Name = "dijeljenje";
            this.dijeljenje.Size = new System.Drawing.Size(75, 23);
            this.dijeljenje.TabIndex = 19;
            this.dijeljenje.Text = "/";
            this.dijeljenje.UseVisualStyleBackColor = true;
            this.dijeljenje.Click += new System.EventHandler(this.dijeljenje_Click);
            // 
            // korijen
            // 
            this.korijen.Location = new System.Drawing.Point(441, 230);
            this.korijen.Name = "korijen";
            this.korijen.Size = new System.Drawing.Size(75, 23);
            this.korijen.TabIndex = 20;
            this.korijen.Text = "sqrt";
            this.korijen.UseVisualStyleBackColor = true;
            this.korijen.Click += new System.EventHandler(this.korijen_Click);
            // 
            // sinus
            // 
            this.sinus.Location = new System.Drawing.Point(441, 269);
            this.sinus.Name = "sinus";
            this.sinus.Size = new System.Drawing.Size(75, 23);
            this.sinus.TabIndex = 21;
            this.sinus.Text = "sin";
            this.sinus.UseVisualStyleBackColor = true;
            this.sinus.Click += new System.EventHandler(this.sinus_Click);
            // 
            // kosinus
            // 
            this.kosinus.Location = new System.Drawing.Point(441, 316);
            this.kosinus.Name = "kosinus";
            this.kosinus.Size = new System.Drawing.Size(75, 23);
            this.kosinus.TabIndex = 22;
            this.kosinus.Text = "cos";
            this.kosinus.UseVisualStyleBackColor = true;
            this.kosinus.Click += new System.EventHandler(this.kosinus_Click);
            // 
            // nat_log
            // 
            this.nat_log.Location = new System.Drawing.Point(441, 357);
            this.nat_log.Name = "nat_log";
            this.nat_log.Size = new System.Drawing.Size(75, 23);
            this.nat_log.TabIndex = 23;
            this.nat_log.Text = "ln";
            this.nat_log.UseVisualStyleBackColor = true;
            this.nat_log.Click += new System.EventHandler(this.nat_log_Click);
            // 
            // logaritam
            // 
            this.logaritam.Location = new System.Drawing.Point(555, 230);
            this.logaritam.Name = "logaritam";
            this.logaritam.Size = new System.Drawing.Size(75, 23);
            this.logaritam.TabIndex = 24;
            this.logaritam.Text = "log";
            this.logaritam.UseVisualStyleBackColor = true;
            this.logaritam.Click += new System.EventHandler(this.logaritam_Click);
            // 
            // izracunaj
            // 
            this.izracunaj.Location = new System.Drawing.Point(509, 134);
            this.izracunaj.Name = "izracunaj";
            this.izracunaj.Size = new System.Drawing.Size(75, 23);
            this.izracunaj.TabIndex = 25;
            this.izracunaj.Text = "Izracunaj";
            this.izracunaj.UseVisualStyleBackColor = true;
            this.izracunaj.Click += new System.EventHandler(this.izracunaj_Click);
            // 
            // clear
            // 
            this.clear.Location = new System.Drawing.Point(509, 173);
            this.clear.Name = "clear";
            this.clear.Size = new System.Drawing.Size(75, 23);
            this.clear.TabIndex = 26;
            this.clear.Text = "Očisti";
            this.clear.UseVisualStyleBackColor = true;
            this.clear.Click += new System.EventHandler(this.clear_Click);
            // 
            // exit
            // 
            this.exit.Location = new System.Drawing.Point(652, 380);
            this.exit.Name = "exit";
            this.exit.Size = new System.Drawing.Size(75, 23);
            this.exit.TabIndex = 27;
            this.exit.Text = "Exit";
            this.exit.UseVisualStyleBackColor = true;
            this.exit.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.exit);
            this.Controls.Add(this.clear);
            this.Controls.Add(this.izracunaj);
            this.Controls.Add(this.logaritam);
            this.Controls.Add(this.nat_log);
            this.Controls.Add(this.kosinus);
            this.Controls.Add(this.sinus);
            this.Controls.Add(this.korijen);
            this.Controls.Add(this.dijeljenje);
            this.Controls.Add(this.mnozenje);
            this.Controls.Add(this.minus);
            this.Controls.Add(this.plus);
            this.Controls.Add(this.polje);
            this.Name = "Form1";
            this.Text = "Znanstveni kalkulator";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox polje;
        private System.Windows.Forms.Button plus;
        private System.Windows.Forms.Button minus;
        private System.Windows.Forms.Button mnozenje;
        private System.Windows.Forms.Button dijeljenje;
        private System.Windows.Forms.Button korijen;
        private System.Windows.Forms.Button sinus;
        private System.Windows.Forms.Button kosinus;
        private System.Windows.Forms.Button nat_log;
        private System.Windows.Forms.Button logaritam;
        private System.Windows.Forms.Button izracunaj;
        private System.Windows.Forms.Button clear;
        private System.Windows.Forms.Button exit;
    }
}

