﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp9
{
    public partial class Form1 : Form
    {
        double prvi;
        string operacija;
        double drugi;
        double rezultat;
        string line;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            

          
        }

        private void button1_Click(object sender, EventArgs e)
        {
            polje.Clear();
            Application.Exit();

        }

        private void polje_TextChanged(object sender, EventArgs e)
        {
           
            
        }

        private void plus_Click(object sender, EventArgs e)
        {
            operacija = "+";
            polje.AppendText("+");

        }

        private void minus_Click(object sender, EventArgs e)
        {
            operacija = "-";
            polje.AppendText("-");
        }

        private void mnozenje_Click(object sender, EventArgs e)
        {
            operacija = "*";
            polje.AppendText("*");
        }

        private void dijeljenje_Click(object sender, EventArgs e)
        {
            operacija ="/";
            polje.AppendText("/");
        }

        private void korijen_Click(object sender, EventArgs e)
        {
            operacija = "sqrt";
        }

        private void sinus_Click(object sender, EventArgs e)
        {
            operacija = "sin";
        }

        private void kosinus_Click(object sender, EventArgs e)
        {

            operacija = "cos";
        }

        private void nat_log_Click(object sender, EventArgs e)
        {
            operacija = "ln";
        }

        private void logaritam_Click(object sender, EventArgs e)
        {
            operacija = "log";
        }

        private void clear_Click(object sender, EventArgs e)
        {
            polje.Clear();
        }

        private void izracunaj_Click(object sender, EventArgs e)
        {
            line = polje.Text;
            string[] parts = line.Split('+', '-', '*', '/', ' ');
            double p;
            double d;
           
            

            switch (operacija)
            {
                case "+":
                    double.TryParse(parts[0], out p);
                    double.TryParse(parts[1], out d);
                    prvi = p;
                    drugi = d;
                    rezultat = prvi + drugi;
                    break;
                case "-":
                    double.TryParse(parts[0], out p);
                    double.TryParse(parts[1], out d);
                    prvi = p;
                    drugi = d;

                    rezultat = prvi - drugi;
                    break;



                case "*":
                    double.TryParse(parts[0], out p);
                    double.TryParse(parts[1], out d);
                    prvi = p;
                    drugi = d;

                    rezultat = prvi* drugi;
                    break;
                case "/":
                    double.TryParse(parts[0], out p);
                    double.TryParse(parts[1], out d);
                    prvi = p;
                    drugi = d;

                    rezultat = prvi/drugi;
                    break;

                case "sin":
                    double.TryParse(parts[0], out p);
                   
                    prvi = p;
                    
                    rezultat =Math.Sin(prvi);
                   
                    break;
                case "cos":
                    double.TryParse(parts[0], out p);
                    
                    prvi = p;
                   
                    rezultat = Math.Cos(prvi);
                    
                    break;
                case "sqrt":
                    double.TryParse(parts[0], out p);
                    
                    prvi = p;
                    
                    rezultat = Math.Sqrt(prvi);
                   
                    break;
                case "ln":
                    double.TryParse(parts[0], out p);
                    
                    prvi = p;
                    
                    rezultat = Math.Log(prvi);

                  


                    break;
                case "log":
                    double.TryParse(parts[0], out p);
                    
                    prvi = p;
                    
                    rezultat = Math.Log10(prvi);
                    break;
            }
            polje.Clear();
            polje.Text = rezultat.ToString();
        }
    }
}
