﻿namespace WindowsFormsApp14
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BTN_PLAY = new System.Windows.Forms.Button();
            this.slovoTB = new System.Windows.Forms.TextBox();
            this.unosBTN = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.TB0 = new System.Windows.Forms.TextBox();
            this.TB1 = new System.Windows.Forms.TextBox();
            this.TB2 = new System.Windows.Forms.TextBox();
            this.TB3 = new System.Windows.Forms.TextBox();
            this.TB4 = new System.Windows.Forms.TextBox();
            this.TB5 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // BTN_PLAY
            // 
            this.BTN_PLAY.Location = new System.Drawing.Point(569, 329);
            this.BTN_PLAY.Name = "BTN_PLAY";
            this.BTN_PLAY.Size = new System.Drawing.Size(70, 46);
            this.BTN_PLAY.TabIndex = 0;
            this.BTN_PLAY.Text = "Play";
            this.BTN_PLAY.UseVisualStyleBackColor = true;
            this.BTN_PLAY.Click += new System.EventHandler(this.BTN_PLAY_Click);
            // 
            // slovoTB
            // 
            this.slovoTB.Location = new System.Drawing.Point(266, 307);
            this.slovoTB.Name = "slovoTB";
            this.slovoTB.Size = new System.Drawing.Size(112, 20);
            this.slovoTB.TabIndex = 1;
            this.slovoTB.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // unosBTN
            // 
            this.unosBTN.Location = new System.Drawing.Point(292, 344);
            this.unosBTN.Name = "unosBTN";
            this.unosBTN.Size = new System.Drawing.Size(58, 31);
            this.unosBTN.TabIndex = 2;
            this.unosBTN.Text = "Unesi";
            this.unosBTN.UseVisualStyleBackColor = true;
            this.unosBTN.Click += new System.EventHandler(this.unosBTN_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(590, 241);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Pokušaji";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // TB0
            // 
            this.TB0.Location = new System.Drawing.Point(160, 86);
            this.TB0.Name = "TB0";
            this.TB0.Size = new System.Drawing.Size(34, 20);
            this.TB0.TabIndex = 4;
            // 
            // TB1
            // 
            this.TB1.Location = new System.Drawing.Point(216, 86);
            this.TB1.Name = "TB1";
            this.TB1.Size = new System.Drawing.Size(34, 20);
            this.TB1.TabIndex = 5;
            // 
            // TB2
            // 
            this.TB2.Location = new System.Drawing.Point(266, 86);
            this.TB2.Name = "TB2";
            this.TB2.Size = new System.Drawing.Size(34, 20);
            this.TB2.TabIndex = 6;
            // 
            // TB3
            // 
            this.TB3.Location = new System.Drawing.Point(316, 86);
            this.TB3.Name = "TB3";
            this.TB3.Size = new System.Drawing.Size(34, 20);
            this.TB3.TabIndex = 7;
            // 
            // TB4
            // 
            this.TB4.Location = new System.Drawing.Point(375, 86);
            this.TB4.Name = "TB4";
            this.TB4.Size = new System.Drawing.Size(34, 20);
            this.TB4.TabIndex = 8;
            // 
            // TB5
            // 
            this.TB5.Location = new System.Drawing.Point(429, 86);
            this.TB5.Name = "TB5";
            this.TB5.Size = new System.Drawing.Size(34, 20);
            this.TB5.TabIndex = 9;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(685, 405);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 10;
            this.button1.Text = "Izlaz";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.TB5);
            this.Controls.Add(this.TB4);
            this.Controls.Add(this.TB3);
            this.Controls.Add(this.TB2);
            this.Controls.Add(this.TB1);
            this.Controls.Add(this.TB0);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.unosBTN);
            this.Controls.Add(this.slovoTB);
            this.Controls.Add(this.BTN_PLAY);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BTN_PLAY;
        private System.Windows.Forms.TextBox slovoTB;
        private System.Windows.Forms.Button unosBTN;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TB0;
        private System.Windows.Forms.TextBox TB1;
        private System.Windows.Forms.TextBox TB2;
        private System.Windows.Forms.TextBox TB3;
        private System.Windows.Forms.TextBox TB4;
        private System.Windows.Forms.TextBox TB5;
        private System.Windows.Forms.Button button1;
    }
}

